$list = Get-Content .\computer_names.txt

foreach ($i in $list) {
  try {
    .\PSTools\PsExec.exe -nobanner \\$i ipconfig

    if ($Error -match "The handle is invalid") {
      echo $i,": could not connect" >> .\pinger.txt
      echo '' >> .\pinger.txt
    }
    elseif ($Error -match "error code 0") {
      echo $i,": error code 0" >> .\pinger.txt
      echo '' >> .\pinger.txt
    }
  }
  catch {
    Write-Host $i,": error"
  }
}
