what does it do?
pinger.ps1 checks a list of machine names for network availability.

skills used:
> powerrshell
> PsTools

requirements:
> powershell
> PsTools

how to run:
1. put machine names in computer_names.txt
2. run pinger.ps1
3. results are logged in pinger.txt
